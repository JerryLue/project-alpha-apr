# path is native to django and allows you to establish a url pattern.
from django.urls import path

# this imports functions from your views.py to pass through the urlpatterns
from accounts.views import user_login, user_logout, signup

# path(location/html path, views function that was imported,
# name=usually same name as views function but can be changed depending
# on how you want to call the url)
urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
