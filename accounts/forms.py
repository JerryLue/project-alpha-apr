# django validation and HTML form handling.
from django import forms


# define a login form using the built in forms model
class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        # uses a widget that determines if the value will be shown when the
        # form is redisplayed after a validation error automatically set to false
        widget=forms.PasswordInput,
    )


# define a Signup form with username, password, and password_confirmation
class SignupForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
