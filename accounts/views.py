# redirect returns an HttpResponseRedirect to the appropriate URL
# for the arguments passed
# render combines a template with a context dictionary and
# returns an HttpResponse object with rendered text
from django.shortcuts import render, redirect

# login saves the user's ID in the session
# any data set during the anonymous session is retained in the
# session after a user logs in
from django.contrib.auth import authenticate, login, logout

# user model is a user profile model from django
from django.contrib.auth.models import User

# imports the sign up forms you created in forms.py
from accounts.forms import LoginForm, SignupForm


# define a function to log people in
def user_login(request):
    # if user request is post
    if request.method == "POST":
        # form is set equal to the login form passing in the user request
        form = LoginForm(request.POST)
        # if the form is valid and follows the form constraints
        if form.is_valid():
            # sets username to the input from the user
            username = form.cleaned_data["username"]
            # sets password to the input from the user
            password = form.cleaned_data["password"]
            # sets a variable user by passing in the above fields
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            # checks if the user credentials mathc an account in the database
            if user is not None:
                # logs in the user
                login(request, user)
                # redirects them to the home page
                return redirect("home")
    # else (if the request is get) set form equal to a blank form
    else:
        form = LoginForm()
    # create a context dictionary using form
    context = {
        "form": form,
    }
    # return redirect
    return render(request, "accounts/login.html", context)


# define a logout function that returns the user to the login page
def user_logout(request):
    logout(request)
    return redirect("login")


# define a signup function so people can make an account
def signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                user.save()
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "the passwords do no match")
    else:
        form = SignupForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
