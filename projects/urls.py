from django.urls import path

from projects.views import list_projects, show_project, create_project

urlpatterns = [
    path("projects/", list_projects, name="list_projects"),
    path("", list_projects, name="home"),
    path("create/", create_project, name="create_project"),
    path("projects/<int:id>/", show_project, name="show_project"),
]
