# imports admin from django
from django.contrib import admin

# import items from the models.py page so you can register them here
from projects.models import Project


# register the project model
@admin.register(Project)
# create a project admin model
class ProjectAdmin(admin.ModelAdmin):
    # display the name, description, owner, and id
    list_display = (
        "name",
        "description",
        "owner",
        "id",
    )
