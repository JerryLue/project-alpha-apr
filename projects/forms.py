# import a built in template from django that is for creating forms
from django.forms import ModelForm

# call the project model you made
from projects.models import Project


# create a class called ProjectForm to allow users to submit new forms
class ProjectForm(ModelForm):
    # create a meta subclass
    class Meta:
        # use the project model made in the models.py and
        #  use those fields in this forms template
        # only add the fields that people should be able to create
        #  so not created on or is completed etc.
        model = Project
        fields = (
            "name",
            "description",
            "owner",
        )
