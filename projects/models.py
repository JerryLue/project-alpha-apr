# imports default models template from django
from django.db import models

# import settings to access user model
from django.conf import settings


# define a "Project" model class with name, description, and owner fields
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    # owner field refers to a django user model
    # foreign key is used to access to data from another model
    owner = models.ForeignKey(
        # this is the recommended way to access the user model
        settings.AUTH_USER_MODEL,
        related_name="projects",
        # when the user is deleted so is the Project model associated with it
        on_delete=models.CASCADE,
        # for existing owners without an project field fill it with null
        null=True,
    )

    # returns Project model objects as the name they are assigned
    # instead of the default object number
    # should now see name instead of id number in admin panel
    def __str__(self):
        return self.name


# MAKE MIGRATION EVERY TIME YOU CHANGE THE MODEL!
