from django.shortcuts import render, redirect

from projects.models import Project

from django.contrib.auth.decorators import login_required

from projects.forms import ProjectForm


@login_required
# define a function to list all project model fields from user
def list_projects(request):
    lists = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": lists,
    }
    return render(request, "projects/list.html", context)


@login_required
# define a function to show details of individual projects
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {
        "show_project": project,
    }
    return render(request, "projects/details.html", context)


@login_required
# define a function to create a new project
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            new_account = form.save(False)
            new_account.owner = request.user
            form.save()
            new_account.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
