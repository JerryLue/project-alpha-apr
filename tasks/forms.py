# import a built in template from django that is for creating forms
from django.forms import ModelForm

from tasks.models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = (
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        )
