from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
# define a function to make a new project task
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
# define a function to show all tasks related to user
def show_my_tasks(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {
        "mine": task_list,
    }
    return render(request, "tasks/list.html", context)
